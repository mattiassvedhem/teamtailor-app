# Simple Isomorphic React example application

The goal was to get an isomorphic React app running with Apollo client & Material UI. All pages are rendered server side on the first entry, for search engines & a better user experience.

Focus has mostly been on integrating the frameworks/libraries, at this stage would probably start writing tests with Jest & Enzyme.

## Frameworks used

- React + Next.js for SSR & conventions
- Express/GraphQL server
- Apollo GraphQL Client
- MaterialUI

## Usage

1. Run `npm install`
1. Run `node graphql-server/server.js` to start the GraphQL server
1. Run `npm run dev` to start the NextJS server
1. Go to `localhost:3000`
