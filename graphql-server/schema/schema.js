const graphql = require("graphql");
const axios = require("axios");
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLSchema,
  GraphQLList
} = graphql;

axios.defaults.baseURL = "https://api.teamtailor.com/v1";
axios.defaults.headers.common["Authorization"] =
  "Token token=Rc8AzlwzE7EI3KF5sEoeOlCMGwiWXzNXXCLlAD1h";
axios.defaults.headers.common["X-Api-Version"] = "20161108";
axios.defaults.headers.common["Accept"] = "application/vnd.api+json";

const logErrors = error => {
  if (error.response) {
    console.error(error.response.data);
    console.error(error.response.status);
    console.error(error.response.headers);
  } else if (error.request) {
    console.error(error.request);
  } else {
    console.error("Error", error.message);
  }
  console.error(error.config);
};

const JobType = new GraphQLObjectType({
  name: "JobType",
  fields: () => ({
    id: { type: GraphQLString },
    attributes: { type: JobAttributesType },
    locations: {
      type: new GraphQLList(LocationType),
      resolve(parentValue) {
        if (!parentValue.relationships.locations) return [];
        return axios
          .get(parentValue.relationships.locations.links.related)
          .then(resp => {
            return resp.data.data;
          })
          .catch(function(error) {
            // Implement error handling here, for now just log.
            logErrors(error);
          });
      }
    }
  })
});

const JobAttributesType = new GraphQLObjectType({
  name: "JobAttributesType",
  fields: () => ({
    title: { type: GraphQLString },
    pitch: { type: GraphQLString },
    body: { type: GraphQLString },
    tags: { type: new GraphQLList(GraphQLString) }
  })
});

const LocationType = new GraphQLObjectType({
  name: "LocationType",
  fields: () => ({
    id: { type: GraphQLString },
    attributes: { type: LocationAttributesType }
  })
});

const LocationAttributesType = new GraphQLObjectType({
  name: "LocationAttributesType",
  fields: () => ({
    adress: { type: GraphQLString },
    city: { type: GraphQLString },
    country: { type: GraphQLString },
    headquarters: { type: GraphQLBoolean },
    country: { type: GraphQLString },
    lat: { type: GraphQLString },
    long: { type: GraphQLString },
    name: { type: GraphQLString },
    phone: { type: GraphQLString },
    zip: { type: GraphQLString }
  })
});

const CompanyType = new GraphQLObjectType({
  name: "CompanyType",
  fields: () => ({
    id: { type: GraphQLString },
    attributes: { type: CompanyAttributesType }
  })
});

const CompanyAttributesType = new GraphQLObjectType({
  name: "CompanyAttributesType",
  fields: () => ({
    name: { type: GraphQLString },
    website: { type: GraphQLString }
  })
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    jobs: {
      type: new GraphQLList(JobType),
      args: { id: { type: GraphQLString } },
      resolve(parentValue, args) {
        return axios
          .get("/jobs")
          .then(resp => {
            return resp.data.data;
          })
          .catch(function(error) {
            // Implement error handling here, for now just log.
            logErrors(error);
          });
      }
    },
    job: {
      type: JobType,
      args: { id: { type: GraphQLString } },
      resolve(parentValue, args) {
        return axios
          .get(`/jobs/${args.id}`)
          .then(resp => resp.data.data)
          .catch(function(error) {
            // Implement error handling here, for now just log.
            logErrors(error);
          });
      }
    },
    company: {
      type: CompanyType,
      resolve(parentValue, args) {
        return axios
          .get("/company")
          .then(resp => resp.data.data)
          .catch(function(error) {
            // Implement error handling here, for now just log.
            logErrors(error);
          });
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery
});
