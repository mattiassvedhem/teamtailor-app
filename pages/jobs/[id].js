import React from "react";
import ReactDOM from "react-dom";
import Head from "next/head";
import { useRouter } from "next/router";
import { withApollo } from "lib/apollo";
import { Button } from "@material-ui/core";

import Query from "components/Query";
import JobDetail from "components/JobDetail";
import PageNavButton from "components/PageNavButton";
import fetchJob from "queries/fetchJob";

const JobPage = () => {
  const router = useRouter();

  return (
    <React.Fragment>
      <PageNavButton path="/" />
      <Query query={fetchJob} id={router.query.id}>
        {({ data: { job } }) => {
          return (
            <React.Fragment>
              <Head>
                <title>{job.attributes.title} - Teamtailor</title>
              </Head>
              <JobDetail job={job} />
            </React.Fragment>
          );
        }}
      </Query>
    </React.Fragment>
  );
};

export default withApollo(JobPage);
