import React from "react";
import ReactDOM from "react-dom";
import { withApollo } from "lib/apollo";
import {
  CssBaseline,
  Paper,
  Container,
  AppBar,
  Toolbar,
  Button,
  ThemeProvider
} from "@material-ui/core";

import JobList from "components/JobList";
import Query from "components/Query";
import PageNavButton from "components/PageNavButton";
import fetchJobs from "queries/fetchJobs";

const IndexPage = () => {
  return (
    <React.Fragment>
      <PageNavButton path="/" />
      <Query query={fetchJobs}>
        {({ data: { jobs } }) => {
          return <JobList jobs={jobs} />;
        }}
      </Query>
    </React.Fragment>
  );
};

export default withApollo(IndexPage);
