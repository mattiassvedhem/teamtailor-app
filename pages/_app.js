import React from "react";
import App from "next/app";
import Head from "next/head";
import ReactDOM from "react-dom";
import { CssBaseline, Container, ThemeProvider } from "@material-ui/core";

import Header from "components/Header";
import theme from "material-ui/theme";

// Overrides the default next.js app
class MyApp extends App {
  componentDidMount() {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
        <Head>
          <title>Jobs - Teamtailor</title>
          <link rel="shortcut icon" href="/favicon.ico" />
        </Head>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Header />
          <Container>
            <Component {...pageProps} />
          </Container>
        </ThemeProvider>
      </React.Fragment>
    );
  }
}

export default MyApp;
