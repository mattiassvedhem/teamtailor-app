import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#f43f85"
    },
    secondary: {
      main: "#33373b"
    },
    contrastThreshold: 3,
    tonalOffset: 0.2
  },
  typography: {
    h1: {
      fontSize: 40
    }
  }
});

export default theme;
