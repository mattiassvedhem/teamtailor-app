import gql from "graphql-tag";

export default gql`
  {
    jobs {
      id
      attributes {
        title
        pitch
      }
      locations {
        id
        attributes {
          city
        }
      }
    }
  }
`;
