import gql from "graphql-tag";

export default gql`
  query JobQuery($id: String!) {
    job(id: $id) {
      id
      attributes {
        title
        pitch
        body
        tags
      }
      locations {
        id
        attributes {
          adress
          city
          country
          headquarters
          country
          lat
          long
          name
          phone
          zip
        }
      }
    }
  }
`;
