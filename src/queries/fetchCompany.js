import gql from "graphql-tag";

export default gql`
  {
    company {
      id
      attributes {
        name
        website
      }
    }
  }
`;
