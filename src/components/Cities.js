import React from "react";

const Cities = props => {
  const cities = props.locations
    .map(({ attributes }) => {
      return attributes.city;
    })
    .join(", ");

  return <React.Fragment>{cities}</React.Fragment>;
};

export default Cities;
