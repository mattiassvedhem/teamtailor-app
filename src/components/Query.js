import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { CircularProgress } from "@material-ui/core";

// A wrapper component for creating GraphQL queries with Apollo hooks with a graphical loader.
// Check if there's some better way of doing this, feels like it shouldn't be handled by a component.
const Query = ({ children, query, id }) => {
  const { data, loading, error } = useQuery(query, {
    variables: { id: id }
  });

  if (loading) {
    return (
      <CircularProgress
        style={{
          position: "absolute",
          marginLeft: "50%",
          width: "40px",
          left: "-20px",
          top: "150px"
        }}
      />
    );
  }

  if (error) return <p>Error: {JSON.stringify(error)}</p>;

  return children({ data });
};

export default Query;
