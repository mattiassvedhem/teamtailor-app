import React from "react";
import { Box, Button } from "@material-ui/core";
import Router from "next/router";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    marginTop: "20px",
    marginBottom: "20px"
  }
});

const PageNavButton = props => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Button onClick={() => Router.push(props.path)} color="inherit">
        Back to all jobs
      </Button>
    </Box>
  );
};

export default PageNavButton;
