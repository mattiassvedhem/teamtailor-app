import React, { Component } from "react";
import Link from "next/link";
import { Paper, Grid, List, ListItem, ListItemText } from "@material-ui/core";
import JobListItem from "components/JobListItem";
import PageSheet from "components/layout/PageSheet";

class JobList extends Component {
  renderJobs() {
    return this.props.jobs.map(({ id, attributes, locations }) => {
      return (
        <JobListItem
          key={id}
          id={id}
          attributes={attributes}
          locations={locations}
        />
      );
    });
  }

  render() {
    return (
      <PageSheet>
        <List>{this.renderJobs()}</List>
      </PageSheet>
    );
  }
}

export default JobList;
