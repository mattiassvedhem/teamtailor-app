import React from "react";
import { Grid, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    padding: "50px",
    marginBottom: "150px",
    "& img": {
      maxWidth: "100%"
    },
    "& figure": {
      margin: "20px 0"
    }
  }
});

const PageSheet = ({ children }) => {
  const classes = useStyles();
  return (
    <Paper className={classes.root} elevation={1}>
      <Grid container>
        <Grid xs={12} md={12} item>
          {children}
        </Grid>
      </Grid>
    </Paper>
  );
};

export default PageSheet;
