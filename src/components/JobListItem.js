import React, { Component } from "react";
import Link from "next/link";
import { ListItem, ListItemText } from "@material-ui/core";
import PlaceIcon from "@material-ui/icons/Place";
import Router from "next/router";
import Cities from "components/Cities";

const JobListItem = ({ id, attributes, locations }) => {
  return (
    <ListItem
      button
      className="collection-item"
      onClick={() => Router.push("/jobs/[id]", `/jobs/${id}`)}
    >
      <ListItemText
        primary={attributes.title}
        secondary={<Cities locations={locations} />}
      />
    </ListItem>
  );
};

export default JobListItem;
