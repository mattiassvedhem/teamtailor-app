import React from "react";
import { AppBar, Toolbar, Button } from "@material-ui/core";
import Router from "next/router";

import Logo from "../assets/svg/teamtailor-logo-small.svg";

const Header = () => {
  return (
    <AppBar position="static" color="secondary">
      <Toolbar>
        <Logo />
        <Button onClick={() => Router.push("/")} color="inherit">
          Jobs
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
