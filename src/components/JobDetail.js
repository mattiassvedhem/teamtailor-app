import React, { Component, Fragment } from "react";
import { graphql } from "react-apollo";
import { Box, Paper, Button, Grid, Typography } from "@material-ui/core";
import PageSheet from "components/layout/PageSheet";
import Cities from "components/Cities";

const JobDetail = ({ job }) => {
  return (
    <PageSheet>
      <Typography variant="h1">{job.attributes.title}</Typography>
      <Cities locations={job.locations} />
      <Box dangerouslySetInnerHTML={{ __html: job.attributes.body }}></Box>
      <Button variant="contained" color="primary">
        Apply for this job!
      </Button>
    </PageSheet>
  );
};

export default JobDetail;
