const path = require("path");

// Aliases to be able to use absolute import paths
module.exports = {
  webpack(config, options) {
    config.resolve.alias["components"] = path.join(__dirname, "src/components");
    config.resolve.alias["queries"] = path.join(__dirname, "src/queries");
    config.resolve.alias["lib"] = path.join(__dirname, "src/lib");
    config.resolve.alias["material-ui"] = path.join(
      __dirname,
      "src/material-ui"
    );
    config.resolve.alias["assets"] = path.join(__dirname, "src/assets");
    return config;
  }
};
